@extends('layouts.templateBase')

@section('titulo')
    Prueba dacode
    @parent
@stop

@section('sidebar')
    @parent
@stop

@section('seccion')
    Acortador URLs
    @parent
@stop

@section('content')
    <div class="row">
        <div class="col-md-12 mt-2">
            <base-acortadores></base-acortadores>
        </div>
    </div>
@stop





