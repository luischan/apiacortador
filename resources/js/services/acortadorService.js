export default {
    generarAcortadorURLs : (request)=> {
        return axios.post('api/acortador', request).then((response)=>{
            return Promise.resolve(response.data)
        }).catch((error)=>{
            return Promise.reject(error);
        })
    },

    obtenerURLs:()=>{
        return axios.get('api/acortador/urls', null).then((response)=>{
            return Promise.resolve(response.data)
        }).catch((error)=>{
            return Promise.reject(error);
        })
    },

    uploadFileUrl:(formData)=>{
        return axios.post('api/acortador/file', formData).then((response)=>{
            return Promise.resolve(response.data)
        }).catch((error)=>{
            return Promise.reject(error);
        })
    }
}