
## Requerimientos

- Apache
- PHP > 7.1
- Node.js v8.10.0
- Mysql
- NPM v3.5.2
- Composer v-1-6-5
- PostMan


## Instalacion

- clonar el repositorio
- Correr el comanto 'composer install' para descargar dependencias laravel
- Correr el comando 'npm install' para descargar todas las dependencias js y css
- Agregar persimos a las carpeta storage y todas las subcarpetas
- Crear una base de datos con el nombre 'acortador' con Mysql
- Configurar en .ENV para la conexion de base de datos en caso de utilizar otro gestor de base de datos, puede encontrar informacion en la pagina de laravel.
- Correr las migraciones con el comando: 'php artisan migrate'

## ENDPOINT ACORTADORES

- Importar el archivo 'Acortadores.postman.json' que se encuentra en la raiz en Postman
- configurar el entorno la variable urlServer. Ejemplo: http://localhost/acortadorAPI/public/api/
- al configurar corectamente ya deberia obtener las respuestas del server

## Ejecutar

- Ejecutar el comando 'npm run dev' para generar los archivos js y css publicos Y poder correr el proyecto

## Rutas Vistas

{{host}}/acortadorAPI/public/acortador para la vista de acortadores, subir archivo, enviar multiples y listado
{{host}}/acortadorAPI/public/matrices para la vista de matrices, recorridos.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
