<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'acortador'], function () {
    Route::post('/', 'AcortadoresController@store');
    Route::post('/file', 'AcortadoresController@storeFile');
    Route::get('/redirect/{url}', 'AcortadoresController@redirect')->name('urlRedirect');
    Route::get('/urls', 'AcortadoresController@listUrls');
    Route::get('/{id}', 'AcortadoresController@get');
});