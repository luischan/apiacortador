<?php

namespace App\Http\Controllers;

use App\Services\interfaces\IAcortadorService;
use App\Services\validator\interfaces\IValidatorAcortador;
use Illuminate\Http\Request;

class AcortadoresController extends Controller
{

    protected $acortadorService;
    protected $acortadorValidator;

    public function __construct(IAcortadorService $acortadorService, IValidatorAcortador $validatorAcortador)
    {
        $this->acortadorService = $acortadorService;
        $this->acortadorValidator = $validatorAcortador;
    }

    public function index()
    {
        return view('acortadores');
    }

    public function store(Request $request){
        $this->acortadorValidator->validateStore($request);
        return $this->acortadorService->store($request);
    }

    public function storeFile(Request $request){
        $this->acortadorValidator->validateStoreFile($request);
        return $this->acortadorService->storeFile($request);
    }

    public function redirect($urlCode) {
        $this->acortadorValidator->validateRedirect($urlCode);
        return $this->acortadorService->redirect($urlCode);
    }

    public function listUrls()
    {
        return $this->acortadorService->listaUrls();
    }

    public function get($id)
    {
        $this->acortadorValidator->validateGet($id);
        return $this->acortadorService->get($id);
    }
}
