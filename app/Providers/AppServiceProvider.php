<?php

namespace App\Providers;

use App\Services\implement\AcortadorService;
use App\Services\interfaces\IAcortadorService;
use App\Services\validator\implement\ValidatorAcortador;
use App\Services\validator\interfaces\IValidatorAcortador;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(128);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IAcortadorService::class, AcortadorService::class);
        $this->app->bind(IValidatorAcortador::class, ValidatorAcortador::class);

    }
}
