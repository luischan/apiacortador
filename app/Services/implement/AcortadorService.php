<?php
/**
 * Created by PhpStorm.
 * User: sunwise
 * Date: 29/10/18
 * Time: 11:18 AM
 */

namespace App\Services\implement;

use App\Services\interfaces\IAcortadorService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use App\Link;
use PhpParser\Node\Expr\Array_;

class AcortadorService implements IAcortadorService
{

    public function store($request)
    {
        $acortadorAreglo = Array();
        $acortador = null;
        $urls = $request->url;

        if(!is_array($urls)){
            $acortador = $this->crearAcortador($urls);
            $LinkAcortadores = (object) array(
                'id'=> $acortador->id,
                'url' => $acortador->url,
                'code' => route('urlRedirect',['url'=>$acortador->code])
            );
            array_push($acortadorAreglo, $LinkAcortadores);

        } else{
            foreach ($urls as $url){
                $acortador = $this->crearAcortador($url);
                $LinkAcortadores = (object) array(
                    'id'=> $acortador->id,
                    'url' => $acortador->url,
                    'code' => route('urlRedirect',['url'=>$acortador->code])
                );
                array_push($acortadorAreglo, $LinkAcortadores);
            }
        }

        return Response()->json($acortadorAreglo);
    }

    public function storeFile($request)
    {
        $acortadorAreglo = Array();
        $file =  file($request->file('urls'));

        foreach ($file as $line){
            $url = str_replace(array("\r", "\n"), '', $line);
            $acortador = $this->crearAcortador($url);
            $LinkAcortadores = (object) array(
                'id'=> $acortador->id,
                'url' => $acortador->url,
                'code' => route('urlRedirect',['url'=>$acortador->code])
            );
            array_push($acortadorAreglo, $LinkAcortadores);
        }
        return Response()->json($acortadorAreglo);
    }

    public function crearAcortador($url){
        $acortador = Link::where('url','=', $url)->first();
        if(is_null($acortador)) {
            do {
                $shortUrl = str_random(10);
            } while(Link::where('code','=',$shortUrl)->count() > 0);
            $acortador = new Link();
            $acortador->url = $url;
            $acortador->code = $shortUrl;
            $acortador->save();
        }
        return $acortador;
    }

    /**
     * @param $acortadorId
     */
    public function get($acortadorId)
    {
        $acortador = Link::find($acortadorId);
        if(!is_null($acortador)){
            $acortadors = (object) array(
                'id'=> $acortador->id,
                'url' => $acortador->url,
                'code' => route('urlRedirect',['url'=>$acortador->code])
            );
            return Response()->json($acortadors);
        } else{
            return Response()->json('Ningun acortador encontrado');
        }
    }

    /**
     * @param $urlRedirect
     */
    public function redirect($urlRedirect)
    {
        $acortador = Link::where('code','=', $urlRedirect)->first();
        if(!is_null($acortador)){
            return response()->redirectTo($acortador->url);
        } else{
            $error = 'Ningun acortador encontrado' ;
            throw new \Exception($error);
        }
    }

    public function listaUrls(){
        $acortador = Link::all();
        $acortadorAreglo = Array();

        if(!is_null($acortador)){
            foreach ($acortador as $link){
                $acortadors = (object) array(
                    'id'=> $link->id,
                    'url' => $link->url,
                    'code' => route('urlRedirect',['url'=>$link->code])
                );
                array_push($acortadorAreglo, $acortadors);
            }
            return Response()->json($acortadorAreglo);
        } else{
            return Response()->json($acortador);
        }
    }
}