<?php
/**
 * Created by PhpStorm.
 * User: sunwise
 * Date: 29/10/18
 * Time: 11:17 AM
 */

namespace App\Services\interfaces;


use Illuminate\Http\Request;

interface IAcortadorService
{
    /**
     * @param Request $request
     */
    public function store($request);

    /**
     * @param Request $request
     */
    public function storeFile($request);

    /**
     * @param $acortadorId
     */
    public function get($acortadorId);

    /**
     * @param $urlRedirect
     */
    public function redirect($urlRedirect);

    public function listaUrls();
}