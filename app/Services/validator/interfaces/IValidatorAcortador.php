<?php
/**
 * Created by PhpStorm.
 * User: sunwise
 * Date: 29/10/18
 * Time: 09:20 PM
 */

namespace App\Services\validator\interfaces;


use Illuminate\Http\Request;

interface IValidatorAcortador
{
    public function validate($attributes, $rules, $messages = array(), $data = null);
    public function cleanRequest(Request $request, $attributes);
    public function getAcceptedAttributes(Request $request, $requestAttributes);
    public function validateStore(Request $request);
    public function validateStoreFile(Request $request);
    public function validateRedirect($urlCode);
    public function validateGet($get);
}