<?php
/**
 * Created by PhpStorm.
 * User: sunwise
 * Date: 29/10/18
 * Time: 09:19 PM
 */

namespace App\Services\validator\implement;

use App\Exceptions\apiValidation;
use App\Services\validator\interfaces\IValidatorAcortador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ValidatorAcortador implements IValidatorAcortador
{
    public function validate($attributes, $rules, $messages = array(), $data = null){
        if(!isset($attributes[0])){
            $validator = Validator::make($attributes, $rules, $messages);

            if($validator->fails()){
                throw new apiValidation($validator->errors()->messages());
            }
        }
        else{
            $invalidos = array();
            foreach ($attributes as $key=> $attribute){
                $validator = Validator::make($attribute, $rules, $messages);
                $valido = $validator->passes();
                if(!$valido){
                    $invalidos[] = [
                        "index"=>$key,
                        "errors" => $validator->errors()->getMessages()
                    ];
                }
            }

            if(isset($data["attribute"]) && !empty($data["attribute"]) &&!empty($invalidos)){
                $invalidos = [$data["attribute"]=>$invalidos];
            }

            if(isset($data["parentIndex"]) && !empty($data["parentIndex"]) &&!empty($invalidos)){
                $invalidos = [
                    "index"=>$data["parentIndex"],
                    "errors" => $invalidos
                ];
            }

            if(!empty($invalidos)){
                throw new ApiValidation($invalidos);
            }
        }
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->url;
        $rules = [
            'url' => ['url', 'max:256', 'required'],
        ];
        if(is_array($attributes)){
            foreach ($attributes as $index=>$url) {

                $urlV = compact('url');

                $this->validate($urlV, $rules, [], [
                        "parentIndex" => $index,
                        "attribute" =>'urls'
                    ]
                );
            }
        } else {
            $this->validate($request->all(), $rules);
        }
    }

    public function validateStoreFile(Request $request)
    {
        $attributes = $request->all();
        $msj = ['archivo no admitido'];

        $rules = [
            'urls' => ['file', 'mimetypes:text/plain', 'required'],
        ];

        $this->validate($attributes, $rules, $msj);
    }

    public function validateGet($id){
        $attributes = compact("id");
        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'exists:links,id'
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateRedirect($code){

        $attributes = compact("code");

        $rules = [
            'code' => [
                'bail',
                'required',
                'string',
                'exists:links,code'
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function cleanRequest(Request $request, $attributes){
        return $request->replace($attributes);
    }

    public function getAcceptedAttributes(Request $request, $requestAttributes){
        return $request->only(
            array_keys($requestAttributes)
        );
    }
}