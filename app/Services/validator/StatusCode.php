<?php
/**
 * Created by PhpStorm.
 * User: sunwise
 * Date: 29/10/18
 * Time: 09:11 PM
 */

namespace App\Services\validator;

use function array_combine;
use function array_keys;
use function array_merge;
use function array_values;
use Symfony\Component\HttpFoundation\Response;

class statusCode  extends Response
{
    const SUNWISE_UNKNOWN_EXCEPTION = 0;

    public static $statusTexts = array(
        0 => "SUNWISE_UNKNOWN_EXCEPTION"
    );

    public static function getStatusTexts($key = null, $default = null){
        $keys = array_merge(array_keys(self::$statusTexts), array_keys(parent::$statusTexts));
        $values = array_merge(array_values(self::$statusTexts), array_values(parent::$statusTexts));
        return array_get(array_combine($keys, $values), $key, $default);
    }
}